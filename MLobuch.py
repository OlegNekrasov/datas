import os.path

import numpy as np
import pandas as pd

from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import mean_absolute_error, mean_absolute_percentage_error
from sklearn import preprocessing
import lightgbm
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import GridSearchCV


path = '/home/mn/PycharmProjects/DataP'
df = pd.read_csv(os.path.join(path, "diamonds_moded.csv"), sep=';')
print(df.shape)

print(df.head())

print(df.isna().sum())

df['color'].fillna(df['color'].mode()[0], inplace=True)

print(df.isna().sum())

cat_columns = [cname for cname in df.columns if df[cname].dtype == "object"]
encoder = preprocessing.LabelEncoder()

for col in cat_columns:
    df[col] = encoder.fit_transform(df[col])

print(df)
X = df.drop('price', axis=1)
y = df['price']
X_train, X_valid, y_train, y_valid = train_test_split(X, y, train_size=0.2, random_state=1)

# Linear Regression
# lr =LinearRegression()
# scores = cross_val_score(lr, X_train, y_train, cv=5, scoring='neg_mean_absolute_error')
# print("Linear Reagression cross validation MAE:", - np.mean(scores))

# Decision Tree
# dt = DecisionTreeRegressor()
# scores = cross_val_score(dt, X_train, y_train, cv=5, scoring='neg_mean_absolute_error')
# print('Decision Tree cross validation MAE:', - np.mean(scores))

# Random Forest
# rf = RandomForestRegressor(random_state=0)
# scores = cross_val_score(rf, X_train, y_train, cv=5, scoring='neg_mean_absolute_error')
# print('Random Forest cross validation MAE:', - np.mean(scores))

# LightGBM
# lgb = lightgbm.LGBMRegressor(random_state=0)
# scores = cross_val_score(lgb, X_train, y_train, cv=5, scoring='neg_mean_absolute_error')
# print('Cross validation MAE:', - np.mean(scores))


def cv_params(model, param_grid):
    scoring = 'neg_mean_absolute_error'

    opt_params = GridSearchCV(
        estimator=model,           # Модель
        param_grid=param_grid,     # Параметры
        scoring=scoring,           # Стратегия валидации
        cv=5,                      # Количество слоев кросс валидации
        n_jobs=-1)                 # Количество потоков для обучения, -1 = все

    opt_params.fit(X_train, y_train)
    params = opt_params.best_params_
    bast_score = opt_params.best_score_

    print(f'Best score: {round(-bast_score, 2)}')
    print(f'Best parameters: {params}\n')

    return params


# LightGBM
lgb_param_grid = {
                'max_depth': [9, -1],              # Максимальная глубина дерева
                'num_leaves': [39, 40, 41],                 # Максимальное кол-во листьем на дереве
                'n_estimators': [279, 280, 281]    # Количество деревьев
                }

lgb_clean = lightgbm.LGBMRegressor(random_state=1)
lgb_params = cv_params(lgb_clean, lgb_param_grid)

# Random Forest
rf_param_grid = {
               'max_depth': [20, 25],
               'n_estimators': [500, 800]
                }

# rf_clean = RandomForestRegressor(random_state=1)
# rf_params = cv_params(rf_clean, rf_param_grid)

# Итоговая модель LightGBM
# Обучаем на лучших параметрах, смотрим MAPE
lgb = lightgbm.LGBMRegressor(**lgb_params)
lgb.fit(X_train, y_train)

preds = lgb.predict(X_valid)

print(f'MAPE: {round(mean_absolute_percentage_error(y_valid, preds) * 100, 2)}%')
print(f'MAE: {round(mean_absolute_error(y_valid, preds), 2)}')

results = pd.DataFrame({'Model': np.round(preds), 'Actual': y_valid})
results = results.reset_index().drop('index', axis=1)
print(results.head(15))
print(lgb.fit(X, y))
